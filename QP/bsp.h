//$file${.::bsp.h} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//
// Model: myBlinkyQM.qm
// File:  ${.::bsp.h}
//
// This code has been generated by QM 4.5.1 (https://www.state-machine.com/qm).
// DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
//$endhead${.::bsp.h} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#ifndef BSP_H
#define BSP_H

// a very simple Board Support Package (BSP) -----========--------------------
class BSP {
public:
    enum { TICKS_PER_SEC = 100 }; // numer of clock ticks in a second
    static void init(void);
    static void ledOff(void);
    static void ledOn(void);
};

enum BlinkySignals {
    TIMEOUT_SIG = QP::Q_USER_SIG, // offset the first signal
    MAX_SIG
};

// active object(s) used in this application -------------------------------
extern QP::QActive * const AO_Blinky; // opaque pointer to the Blinky AO

#endif // BSP_H