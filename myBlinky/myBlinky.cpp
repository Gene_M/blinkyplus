#include <stm32f4xx_hal.h>
#include <stm32_hal_legacy.h>

#include "qpcpp.h"
#include "bsp.h"

#include <iostream> // for cout/cerr
#include <cstdlib>  // for exit()

using namespace QP;
using namespace std;

#ifdef __cplusplus
extern "C"
#endif
	
//void SysTick_Handler(void)
//{
//	HAL_IncTick();
//	HAL_SYSTICK_IRQHandler();
//}


// ask QM to declare the Blinky class ----------------------------------------
//$declare${AOs::Blinky} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//${AOs::Blinky} .............................................................
class Blinky : public QP::QActive {
	private:
		QP::QTimeEvt m_timeEvt;

	public:
		Blinky();

	protected:
		Q_STATE_DECL(initial);
		Q_STATE_DECL(off);
		Q_STATE_DECL(on);
};
//$enddecl${AOs::Blinky} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// instantiate the Blinky active object --------------------------------------
static Blinky l_blinky;
QActive * const AO_Blinky = &l_blinky;

// ask QM to define the Blinky class (including the state machine) -----------
//$skip${QP_VERSION} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// Check for the minimum required QP version
#if (QP_VERSION < 650U) || (QP_VERSION != ((QP_RELEASE^4294967295U) % 0x3E8U))
#error qpcpp version 6.5.0 or higher required
#endif
//$endskip${QP_VERSION} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//$define${AOs::Blinky} vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
//${AOs::Blinky} .............................................................
//${AOs::Blinky::Blinky} .....................................................
Blinky::Blinky()
	: QActive(Q_STATE_CAST(&Blinky::initial))
	, m_timeEvt(this, TIMEOUT_SIG, 0U)
{}

//${AOs::Blinky::SM} .........................................................
Q_STATE_DEF(Blinky, initial) {
	//${AOs::Blinky::SM::initial}
	// arm the private time event to expire in 1/2s
	// and periodically every 1/2 second
	m_timeEvt.armX(BSP::TICKS_PER_SEC * 0.1,
		BSP::TICKS_PER_SEC * 0.1);
	(void)e;  // unused parameter
	return tran(&off);
}
//${AOs::Blinky::SM::off} ....................................................
Q_STATE_DEF(Blinky, off) {
	QP::QState status_;
	switch (e->sig) {
		//${AOs::Blinky::SM::off}
		case Q_ENTRY_SIG : {
			BSP::ledOff();
			status_ = Q_RET_HANDLED;
			break;
		}
		//${AOs::Blinky::SM::off::TIMEOUT}
		case TIMEOUT_SIG : {
			status_ = tran(&on);
			break;
		}
	default: {
			status_ = super(&top);
			break;
		}
	}
	return status_;
}

//${AOs::Blinky::SM::on} .....................................................
Q_STATE_DEF(Blinky, on) {
	QP::QState status_;
	switch (e->sig) {
		//${AOs::Blinky::SM::on}
		case Q_ENTRY_SIG : {
			BSP::ledOn();
			status_ = Q_RET_HANDLED;
			break;
		}
		//${AOs::Blinky::SM::on::TIMEOUT}
		case TIMEOUT_SIG : {
			status_ = tran(&off);
			break;
		}
	default: {
			status_ = super(&top);
			break;
		}
	}
	return status_;
}
//$enddef${AOs::Blinky} ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

int main()
{
	HAL_Init();
	__GPIOG_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = GPIO_PIN_14;
	
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStructure);
	
	QF::init();    // initialize the framework
	BSP::init();   // initialize the BSP

	// start the Blinky active object
	static QEvt const *blinky_queueSto[10];   // event queue buffer for Blinky
	AO_Blinky->start(1U,	// priority of the active object
		blinky_queueSto,	// event queue buffer
		Q_DIM(blinky_queueSto),	// the length of the buffer
		(void *)0, 0U);    // private stack (not used on the desktop)

	return QF::run();   // let the framework run the application
}

void BSP::ledOff(void) 
{ 
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET); 
	cout << "LED OFF" << endl; 
}

void BSP::ledOn(void)  
{
	HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
	cout << "LED ON" << endl;  
}
