///***************************************************************************
// Product: DPP example, STM32F4-Discovery board, preemptive QK kernel
// Last Updated for Version: 6.5.0
// Date of the Last Update:  2019-05-09
//
//                    Q u a n t u m  L e a P s
//                    ------------------------
//                    Modern Embedded Software
//
// Copyright (C) 2005-2019 Quantum Leaps, LLC. All rights reserved.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Alternatively, this program may be distributed and modified under the
// terms of Quantum Leaps commercial licenses, which expressly supersede
// the GNU General Public License and are specifically designed for
// licensees interested in retaining the proprietary status of their code.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// Contact information:
// https://www.state-machine.com
// mailto:info@state-machine.com
//****************************************************************************
#include "qpcpp.hpp"
#include "bsp.hpp"

#include "stm32f4xx.h"      // CMSIS-compliant header file for the MCU used
#include "stm32f4xx_exti.h"
//#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
// add other drivers if necessary...

#ifdef Q_SPY
#error Simple Blinky Application does not provide Spy build configuration
#endif

//Q_DEFINE_THIS_FILE

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
// DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
//

// ISRs used in this project =================================================
extern "C" 
{
	void SysTick_Handler(void);  // prototype
	void SysTick_Handler(void) {
		QK_ISR_ENTRY();    // inform QK about entering an ISR

		QP::QF::TICK_X(0U, &l_SysTick);  // process time events for rate 0
	
		QK_ISR_EXIT();   // inform QK about exiting an ISR
	}

} // extern "C"

// BSP functions =============================================================
void BSP::init(void) {
	// NOTE: SystemInit() already called from the startup code
	//  but SystemCoreClock needs to be updated
	//
	SystemCoreClockUpdate();

	// configure the FPU usage by choosing one of the options...
#if 1
	    // OPTION 1:
	    // Use the automatic FPU state preservation and the FPU lazy stacking.
	    //
	    // NOTE:
	    // Use the following setting when FPU is used in more than one task or
	    // in any ISRs. This setting is the safest and recommended, but requires
	    // extra stack space and CPU cycles.
	    //
	    FPU->FPCCR |= (1U << FPU_FPCCR_ASPEN_Pos) | (1U << FPU_FPCCR_LSPEN_Pos);
#else
	// OPTION 2:
	// Do NOT to use the automatic FPU state preservation and
	// do NOT to use the FPU lazy stacking.
	//
	// NOTE:
	// Use the following setting when FPU is used in ONE task only and not
	// in any ISR. This setting is very efficient, but if more than one task
	// (or ISR) start using the FPU, this can lead to corruption of the
	// FPU registers. This option should be used with CAUTION.
	//
	FPU->FPCCR &= ~((1U << FPU_FPCCR_ASPEN_Pos) | (1U << FPU_FPCCR_LSPEN_Pos));
#endif
}
//............................................................................

// namespace QP **************************************************************
namespace QP {

	// QF callbacks ==============================================================
	void QF::onStartup(void) {
		// set up the SysTick timer to fire at BSP::TICKS_PER_SEC rate
		SysTick_Config(SystemCoreClock / BSP::TICKS_PER_SEC);

		// assign all priority bits for preemption-prio. and none to sub-prio.
		NVIC_SetPriorityGrouping(0U);

		// !!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Assign a priority to EVERY ISR explicitly, see NOTE00.
		// DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
		//

		// kernel AWARE interrupts, see NOTE00
		NVIC_SetPriority(SysTick_IRQn, QF_AWARE_ISR_CMSIS_PRI);
		// ...

		// enable IRQs...
	}

	void QF::onCleanup(void) {
	}
	//............................................................................

	void QK::onIdle(void) {
	}
	//............................................................................

		extern "C" void Q_onAssert(char const *module, int loc) {
		//
		// NOTE: add here your application-specific error handling
		//
		(void)module;
		(void)loc;
		QS_ASSERTION(module, loc, static_cast<uint32_t>(10000U));
		NVIC_SystemReset();
	}
}